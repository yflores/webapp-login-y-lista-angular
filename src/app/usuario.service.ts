import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(
    private http: Http
  ) { }  

  logear(email: string, password: string, app: string): Observable<any> {
    let headers = new Headers();
    headers.append('app', app);
    headers.append('password', password);
    return this.http.put(`https://dev.tuten.cl/TutenREST/rest/user/${email}`, undefined, {headers: headers})
                    .pipe(map((response: any) => response.json()));
  }

  listElementsTable(adminemail: string, email: string, current: boolean, app: string, token: string): Observable<any> {
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('adminemail', adminemail);
    headers.append('app', app);
    headers.append('token', token);
    return this.http.get(`https://dev.tuten.cl/TutenREST/rest/user/${email}/bookings?current=${current}`, {headers: headers})
                    .pipe(map((response: any) => response.json()));
  }
}