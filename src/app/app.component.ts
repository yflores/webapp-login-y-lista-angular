import { Component, ViewChild } from '@angular/core';
import { UsuarioService} from './usuario.service';
import { LoginModel } from './object.model';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  usuario = null;
  hide = true;
  login: LoginModel;
  token: string;
  view: number;
  emailTwo = "contacto@tuten.cl";
  current = true;
  listData: any [];
  displayedColumns: string[] = ['bookingId', 'firstName', 'bookingTime', 'streetAddress', 'bookingPrice'];
  dataSource;

  constructor(
    private usuarioService: UsuarioService
    ) {}  

  ngOnInit() {
    this.login = new LoginModel(undefined, undefined, undefined);
    this.view = 1;
  }  

  logear(login: LoginModel) {
    this.usuarioService.logear(login.email, login.password, login.app)
    .subscribe(response => {
      this.token = response.sessionTokenBck;
      console.log("Salida ->: ", this.token);
      this.usuarioService.listElementsTable(login.email, this.emailTwo, this.current, login.app, this.token)
      .subscribe(responseTwo => {
        this.view = 2;
        this.listData = responseTwo;
        this.dataSource = new MatTableDataSource(this.listData);
        console.log("Lista de elementos ->: ", this.listData);
      });
    });
  }

  applyFilter(filterValue: string) {
    console.log("Epaaaaaaa ->: ", filterValue);
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}